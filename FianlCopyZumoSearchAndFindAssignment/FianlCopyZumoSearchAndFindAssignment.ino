#include <ZumoBuzzer.h>
#include <Pushbutton.h> 
#include <ZumoMotors.h>
#include <QTRSensors.h>
#include <ZumoReflectanceSensorArray.h>
#include <NewPing.h>
#include <Wire.h>
#include <LSM303.h>

#define LED 13                             
#define TRIGGER_PIN  4                                    // Arduino pin tied to trigger pin on ping sensor. 
#define ECHO_PIN     5                                    // Arduino pin tied to echo pin on ping sensor.
#define MAX_DISTANCE 100                                  // Maximum distance we want to ping for (in centimeters). Maximum sensor distance is rated at 400-500cm.
#define STOP()(motors.setSpeeds(0,0))                     // Mini function to stop zumo
#define FORWARD()(motors.setSpeeds(100,100))
#define LEFT()(motors.setSpeeds(-(lwheel + change), (rwheel + change)))
#define RIGHT()(motors.setSpeeds((lwheel + change), -(rwheel + change)))
#define NUM_SENSORS  6  

#define CALIBRATION_SAMPLES 70                            // Number of compass readings to take when calibrating
#define CRB_REG_M_2_5GAUSS 0x60                           // CRB_REG_M value for magnetometer +/-2.5 gauss full scale
#define CRA_REG_M_220HZ    0x1C                           // CRA_REG_M value for magnetometer 220 Hz update rate
#define DEVIATION_THRESHOLD 5                             // Allowed deviation (in degrees) relative to target angle that must be achieved before driving straight
  
unsigned int sensor_values[NUM_SENSORS];
unsigned int pingSpeed = 50;                              // How frequently are we going to send out a ping (in milliseconds). 50ms would be 20 times a second.
unsigned long pingTimer;                                  // Holds the next ping time.

ZumoBuzzer buzzer;
ZumoMotors motors; 
Pushbutton button(ZUMO_BUTTON);                           // pushbutton
ZumoReflectanceSensorArray sensors(QTR_NO_EMITTER_PIN);
NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE);       // NewPing setup of pins and maximum distance.
LSM303 compass;

int timer = 0;
int sensorThreshold = 500;                                // 500 or 100 depending on lighting
int change = 50, lwheel = 80, rwheel = 80;                // change allows motor speed to be increased/decreased, motor speed standard value
int leftCount = 0, rightCount = 0;                        // counts for every time left or right is hit
int roomCount = 0;                                        // basically like a room ID
int hitCount [3];                                         // hit counter array records last 3 hits for sequence checks
String rooms[10];                                         // stores what it has found for each room
bool searchingRoom = false;                               // boolean to know when its stoped searching in a room
bool objectFound = false;                                 // used for finding a person currently no person found.
//bool deadEnd = false;
float startDirection, directionNow;                       // directions for the compass

void setup()
{
  Serial.begin(9600);
  button.waitForButton();
  pingTimer = millis();                                   // Start now.

  // The highest possible magnetic value to read in any direction is 2047
  // The lowest possible magnetic value to read in any direction is -2047
  LSM303::vector<int16_t> running_min = {32767, 32767, 32767}, running_max = {-32767, -32767, -32767};
  unsigned char index;
  Wire.begin();                                           // Wire library?
  compass.init();                                         // LSM303 compass Library 
  compass.enableDefault();                                // Enables accelerometer and magnetometer

  compass.writeReg(LSM303::CRB_REG_M, CRB_REG_M_2_5GAUSS);// +/- 2.5 gauss sensitivity to hopefully avoid overflow problems
  compass.writeReg(LSM303::CRA_REG_M, CRA_REG_M_220HZ);   // 220 Hz compass update rate
  Serial.println("Calibration");
                                                          // To calibrate the magnetometer, the Zumo spins to find the max/min
  LEFT(); //turns the motors left                         // magnetic vectors. This information is used to correct for offsets
                                                          // in the magnetometer data.                                                         
  for(index = 0; index < CALIBRATION_SAMPLES; index ++)
  {
    compass.read();                                       // Take a reading of the magnetic vector and store it in compass.m

    running_min.x = min(running_min.x, compass.m.x);
    running_min.y = min(running_min.y, compass.m.y);

    running_max.x = max(running_max.x, compass.m.x);
    running_max.y = max(running_max.y, compass.m.y);
    
    sensors.calibrate();                                  // calibrates the sensors collecting data light and dark areas 
    delay(50);
  }
  
  STOP();                                                 // stops the motors (mini function)

  // Set calibrated values to compass.m_max and compass.m_min
  compass.m_max.x = running_max.x;
  compass.m_max.y = running_max.y;
  compass.m_min.x = running_min.x;
  compass.m_min.y = running_min.y;

  pinMode(LED, HIGH);                                     // light on 
  hitCount[0] = 9; hitCount[1] = 9; hitCount[2] = 9;      // setting up the hit count to diffearnt values at the beginning 0 is left, 1 is right
  buttonPressAndCountDown();                              // function  that waits for a button press and starts an audable countdown  
  startDirection = fmod(averageHeading(), 360);           // set starting direction (the way the zumo is facing) keeps it stored
  directionNow = startDirection;                          // setting directionNow used for the compass diresction(facing) so the original isn't written over.
  LEFT();                                                 // turns left, this is the first turn to find and hit a wall assuming one on the left.
  delay(600);
}

void loop()
{                                                         
  if (button.isPressed())                                 // if button is pressed, wait for button press to go again
  {                                                                   
    button.waitForRelease();                              // waits for the button to be released
    buttonPressAndCountDown();                            // function to start a countdown before starting 
  }
  timer++;                                                // timing distance traveled between walls adds one each loop through
    
  sensors.read(sensor_values);                            // reads in light and dark what the sensors are picking up
  
  if (sensor_values[1] > sensorThreshold || sensor_values[2] > sensorThreshold)
  { 
    hitCount[2] = hitCount[1];                            // if the sensor is over the sensorThreshold 
    hitCount[1] = hitCount[0];
    hitCount[0] = 0;                                      // 0 = LEFT, 1 = RIGHT this determins wall it hit
    leftCount++;                                          // adds a cound to left showing it has hit the left wall
    rightCount = 0;                                       // resetting the right count 
                                                          // these values are stored in hitCount until they are witten over
    while (sensor_values[1] > sensorThreshold || sensor_values[2] > sensorThreshold)
    { // rotate right 
      RIGHT();                                            // rotating right to zigzag (mini function)
      delay(200);
      sensors.read(sensor_values);                        // keeps checking the sensors for lines
    }
    delay(600);                                           // different delay time used to account for slower motor on the left
    Serial.println("Collision on the Left");              // output current state 
    cornerCounter(leftCount, rightCount);                 // passing values to cornerCounter function that decides what the zumo has just done
    timer = 0;                                            // resets the timer counter determining if it is in a room
  }
  if (sensor_values[4] > sensorThreshold || sensor_values[3] > sensorThreshold)
  {
    hitCount[2] = hitCount[1];                            // same as above
    hitCount[1] = hitCount[0];
    hitCount[0] = 1;                                      // 0 = LEFT, 1 = RIGHT this determins wall it hit
    rightCount++;
    leftCount = 0;
    
    while (sensor_values[4] > sensorThreshold || sensor_values[3] > sensorThreshold)
    { // rotate left
      LEFT();                                             // rotating Left to zigzag (mini function)
      delay(200);
      sensors.read(sensor_values);                        // reading the sensors again
    }                                                     
    delay(500);                                           // different delay time used to account for slower motor on the left
    Serial.println("Collision on the Right");             // outputting current state
    cornerCounter(leftCount, rightCount);                 // function keeping track of collisions and checking for corners
    timer = 0;
  } 

//  ultrasonicSensor();                                    // pings the ultrasonic sensor every loop
  checkTimer();                                          //  
  
   sensorTrue();                                           // if sensors pick anything up and object found has been set to true

   FORWARD();                                             // moves forwards 
   delay(50);                                             // otherwise it will keep going without a real delay 
   sensors.read(sensor_values);                           // keeps reading in vlaues from the sensors 
}

void buttonPressAndCountDown()                            // waits for a button press to count down and turn light on and off
{
  digitalWrite(LED, HIGH);                                // light on 
  button.waitForButton();                                 // waits for button press 
  digitalWrite(LED, LOW);                                 // light off
                                                          // play audible countdown
  for (int i = 0; i < 3; i++)                             // count of 3
  {
    delay(1000);                                          // delay before each note is played  
    buzzer.playNote(NOTE_G(3), 200, 15);                  // plays a note 
  }
  delay(1000);                                                          
  buzzer.playNote(NOTE_G(4), 500, 15);                    // third and final note played
  delay(1000);                                            // done
}

void room()
{  
   float tempDirection;                                   // tempDirection because of the room dont want to loose the corridor direction
   if (hitCount[0] == 0)                                  // 0 = Left, 1  = Right 
   {
     tempDirection = directionNow + 90;                   // so corridor direction + 90  // turns left
     if (tempDirection > 360)                             // if the tempDirection is greater than 360 
     {                                                    
      tempDirection -= 360;                               // take 360 away to get the correct direction
     }
     headingDirection(tempDirection, "RIGHT");            // determins the rotation direction (a function that turns to the direction set)
   }
   else                                                   // else is anything other than 0 so 1 would be RIGHT
   {
    tempDirection = directionNow - 90;                    // so corridor direction - 90  // turns right
    if (tempDirection < 0)                                // if tempDirection is less than 0
    {
      tempDirection += 360;                               // adds 360 to give it the correct angle
    }
    headingDirection(tempDirection, "LEFT");              // determins the rotation direction 
   }
   
   while (sensor_values[1] < sensorThreshold && sensor_values[4] < sensorThreshold)
   {
      ultrasonicSensor();                                 ////////////////////////////////////////////////////////////////////pinging ultrasonic sensor to find an object.
      FORWARD();                                          // moves forwards (mini function)
      sensors.read(sensor_values);
      delay(100);   
   }
   STOP();
   sensorTrue();
   motors.setSpeeds(-lwheel, -rwheel);                    // reverses the motors so the zumo moves backwards
   delay(500);                                            // does this for a delay of 500, not long 
   headingDirection(directionNow, "LEFT");                        
   STOP();
   delay(1000);
   FORWARD();
   delay(100);
   tempDirection = directionNow - 180;                    // 180 rotation
   if (tempDirection < 0)
   {
     tempDirection += 360; 
   }
   headingDirection(tempDirection, "LEFT");
   STOP();
   delay(1000);
   FORWARD();
   delay(500);  
   STOP();
   delay(1000);

   if (hitCount[0] == 0)                                  // 0 = Left, 1  = Right 
   {                                                      // this is okay because the hitCount default values have been written over
     tempDirection = directionNow - 90;                   
     if (tempDirection < 0)
     {
      tempDirection += 360; 
     }
     headingDirection(tempDirection, "RIGHT");            // determins the rotation direction 
   }
   else                                                   // else is anything other than 0 so 1 would be RIGHT, default values won't be 9 still
   {
    tempDirection = directionNow + 90;
    if (tempDirection > 360)
    {
      tempDirection -= 360; 
    }
    headingDirection(tempDirection, "LEFT");              // determins the rotation direction 
   }
   STOP();
   while (sensor_values[1] < sensorThreshold && sensor_values[4] < sensorThreshold && objectFound == false)
   {
      ultrasonicSensor();////////////////////////////////////////////////////////////////////////////////////////////////////
      FORWARD();
      sensors.read(sensor_values);
      delay(100);   
   }
   
   STOP();
   sensors.read(sensor_values);
   while (sensor_values[1] < sensorThreshold && sensor_values[4] < sensorThreshold && objectFound == false)
   {
      ultrasonicSensor();     
      FORWARD();
      sensors.read(sensor_values);
      delay(100);   
   }
   sensorTrue();
   Serial.println("Leaving Room");                        // action update 
   searchingRoom = false;                                 // set searching room to false so that the search is of that room is over
   roomCount ++;                                          // adds a cound to the room array (stores the number of rooms the zumo has been in)
   motors.setSpeeds(-lwheel, -rwheel);                    // reverses
   delay(500);
   headingDirection(directionNow, "LEFT");
   
   delay(5000);
   FORWARD();
   delay(500);
   if (hitCount[0] == 0)                                  // 0 = Left, 1  = Right 
   {
    LEFT();                                               // turns left first turn to find and hit a wall
   }
   else
   {
    RIGHT();
   }
   hitCount[0] = 9; hitCount[1] = 9; hitCount[2] = 9;     // default values are back so the process can start again
   leftCount = 0;
   rightCount = 0;
   delay(600);
   Serial.print("room: ");                                // counting the rooms for optimisation MIGHT GET RID OF THIS  
   Serial.println(roomCount);                             // outputs the room count, counting the rooms for optimisation MIGHT GET RID OF THIS
}   

void sensorTrue(){
  if(objectFound == true)                                 // if sensors pick anything up and object found has been set to true
  {
    rooms[roomCount] = "PersonFound";                // setting a value in the array to personFound for optimisation MIGHT GET RID OF THIS  
    Serial.println("Object Found");                       // prints that it has found something hidden object, person etc.
    STOP();                                               // stop motors
    delay(2000);                                          // delay before moving away
//    float tempDirection = fmod(averageHeading(), 360);    // get the current heading where the zumo is facing and gives it a degrees
//    tempDirection -= 180;
//    if (tempDirection < 0)
//    {
//      tempDirection += 360; 
//    }
//    headingDirection(tempDirection, "LEFT");              
    
    objectFound = false;                                  // setting object found to false so it moves on and can start looking again
  }
}
void checkTimer()
{
  if (timer > 40)                                         // if the timer is greater than 40 then the zumo stopps
  {                                                       
    STOP();
    Serial.print("Stopped timer over 40: ");
    Serial.println(timer);
    Serial.println("Found Room");                         // output progress
    delay(900);
    timer = 0;                                            // resets timer so it doesnt keep stopping in little hops
    Serial.println("Searching");                          // what it is currrently doing
    searchingRoom = true;                                 // searching room to true so it knows its in a room
    room();                                               // room function that contains actions to be performed when in a room.
   }
}

void cornerCounter(int leftCount, int rightCount)
{ 
  if (leftCount >= 3 || rightCount >= 3)                   // if left or right count equles or is greater than 3 then the zumo sensors have hit
  {                                                        // the left wall 3 timese in a row or right three times in a row looking like a dead end
    Serial.println("Dead End");
    //deadEnd = true;
  }
  else if (hitCount[0] == 1 && hitCount[1] == 1) // 0 = LEFT 1 = RIGHT  
  {                                                                  // so LEFT, RIGHT, RIGHT 
    Serial.println("Turning Corner Left");                           // should mean it is turning a corner left 
    Serial.print("Timer turning corner Left: ");
    Serial.println(timer);
    searchingRoom = false;
    // -90 from start
    directionNow -= 90;
    if (directionNow < 0)
    {
      directionNow += 360; 
    }
  }
  else if (hitCount[0] == 0 && hitCount[1] == 0)
  {
    Serial.println("Turning Corner Right");
    Serial.print("Timer turning corner Right: ");
    Serial.println(timer);
    searchingRoom = false;
    directionNow += 90;
    if (directionNow > 360)
    {
      directionNow -= 360; 
    }
  }
  else if (hitCount[0] == 1 && hitCount[1] == 0 || hitCount[0] == 0 && hitCount[1] == 1)
  {
    Serial.println("In a corridor"); 
    Serial.print("Corridor Timer: ");
    Serial.println(timer);
    searchingRoom = false;                                  // if its in a corridor it cant be in a room          
  }
}

void ultrasonicSensor()
{
  if (millis() >= pingTimer) 
  {                                                         // pingSpeed milliseconds since last ping, do another ping.
    pingTimer += pingSpeed;                                 // Set the next ping time.
    sonar.ping_timer(echoCheck);                            // Send out the ping, calls "echoCheck" function every 24uS where you can check the ping status.
  }
}

void echoCheck()                                            // echoCheck is called within the function ultrasonicSensor to check recieved pings and checks for objects 
{ 
  if (sonar.check_timer())                                  // Checking to see if the ping was received.
  { 
    int foundSomething = (sonar.ping_result / US_ROUNDTRIP_CM);
    if (foundSomething <= 15 )                              // setting the object found status to true when something is under the 15cm radious
    {
      Serial.print("Ping: ");
      Serial.print(sonar.ping_result / US_ROUNDTRIP_CM);    // Ping returned, uS result in ping_result, convert to cm with US_ROUNDTRIP_CM.
      Serial.println("cm");
      objectFound = true;                                   // sets objectFound to true if the sensors find something within 15cm
    }
    else
    {
      objectFound = false;                                  // else it stays false and objectFound hasn't found anything
    }
  }
}

// Converts x and y components of a vector to a heading in degrees.
// This function is used instead of LSM303::heading() because we don't
// want the acceleration of the Zumo to factor spuriously into the
// tilt compensation that LSM303::heading() performs. This calculation
// assumes that the Zumo is always level.

template <typename T> float heading(LSM303::vector<T> v)
{
  float x_scaled =  2.0*(float)(v.x - compass.m_min.x) / ( compass.m_max.x - compass.m_min.x) - 1.0;
  float y_scaled =  2.0*(float)(v.y - compass.m_min.y) / (compass.m_max.y - compass.m_min.y) - 1.0;

  float angle = atan2(y_scaled, x_scaled)*180 / M_PI;
  if (angle < 0)
    angle += 360;
  return angle;
}

float relativeHeading(float heading_from, float heading_to) // Yields the angle difference in degrees between two headings
{
  float relative_heading = heading_to - heading_from;

  // constrain to -180 to 180 degree range
  if (relative_heading > 180)
    relative_heading -= 360;
  if (relative_heading < -180)
    relative_heading += 360;

  return relative_heading;
}

float averageHeading()                                      // Average 10 vectors to get a better measurement and help smooth out
{                                                           // the motors' magnetic interference.
  LSM303::vector<int32_t> avg = {0, 0, 0};

  for(int i = 0; i < 10; i ++)
  {
    compass.read();
    avg.x += compass.m.x;
    avg.y += compass.m.y;
  }
  avg.x /= 10.0;
  avg.y /= 10.0; 
  return heading(avg);                                       // avg is the average measure of the magnetic vector.
}

void headingDirection(float target_heading, String direction)
{
  float heading, relative_heading;                           // where it is going/facing atm.
  int speed;

  //averageHeading = where it came from 
  //relativeHeading = direction its going/facing
  //targetHeading = where it wants to go
  heading = averageHeading();// Heading is given in degrees away from the magnetic vector
  relative_heading = relativeHeading(heading, target_heading);
  
  while(!(abs(relative_heading) < DEVIATION_THRESHOLD))
  {
    // To avoid overshooting, the closer the Zumo gets to the target
    // heading, the slower it should turn. Set the motor speeds to a
    // minimum base amount plus an additional variable amount based
    // on the heading difference.
    ultrasonicSensor();
    sensorTrue();
    speed = (lwheel + change)*relative_heading/180;
    speed = abs(speed) + 80;                            // creates the absolute value so it will be positive
    
    if (direction == "LEFT")                            // this section determins if the zumo turns left or right 
    {
      motors.setSpeeds(-speed, speed);
    }
    else
    {
      motors.setSpeeds(speed, -speed);
    }
    delay(100);
    relative_heading = relativeHeading(averageHeading(), target_heading); 
  }
  STOP();
}

//bool journeyDefine()
//{
//  if (deadEnd == true)
//  {
//    Serial.println(" Returning to base");
//    // link to optimasation function
//  }
//}
  // function 
  // have a system that defines the jurney e.g. first trip true/false, on the way back true/false 
  // then if on the way back and entered a room with a person turn on LED pin 13 or something
