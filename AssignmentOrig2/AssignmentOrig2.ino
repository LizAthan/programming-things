#include <ZumoBuzzer.h>
#include <Pushbutton.h> 
#include <ZumoMotors.h>
#include <QTRSensors.h>
#include <ZumoReflectanceSensorArray.h>
#include <NewPing.h>

#define LED 13                             
#define TRIGGER_PIN  4                                    // Arduino pin tied to trigger pin on ping sensor. 
#define ECHO_PIN     5                                    // Arduino pin tied to echo pin on ping sensor.
#define MAX_DISTANCE 100                                  // Maximum distance we want to ping for (in centimeters). Maximum sensor distance is rated at 400-500cm.
#define STOP() (motors.setSpeeds(0,0))                    // Mini function to stop zumo
#define NUM_SENSORS  6  
  
unsigned int sensor_values[NUM_SENSORS];
unsigned int pingSpeed = 50;                              // How frequently are we going to send out a ping (in milliseconds). 50ms would be 20 times a second.
unsigned long pingTimer;                                  // Holds the next ping time.

ZumoBuzzer buzzer;
ZumoMotors motors; 
Pushbutton button(ZUMO_BUTTON);                           // pushbutton
ZumoReflectanceSensorArray sensors(QTR_NO_EMITTER_PIN);
NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE);       // NewPing setup of pins and maximum distance.

int sensorThreshold = 500;                                // 500 or 100 depending on lighting
int change = 50, lwheel = 80, rwheel = 80;                // change allows motor speed to be increased/decreased, motor speed standard value
int leftCount = 0, rightCount = 0;                        // counts for every time left or right is hit
int hitCount [3];                                         // hit counter array records last 3 hits for sequence checks
bool objectFound = false;                                 // used for finding a person currently no person found.
//bool deadEnd = false;


void setup()
{
  Serial.begin(9600);
  button.waitForButton();
  pingTimer = millis();                                   // Start now.
  
  // Calibration code I didn't need but now seem to need
  unsigned long startTime = millis();
  
  while(millis() - startTime < 10000)                     // make the calibration take 10 seconds
  {                                                       // motor/setSpeeds(Left, Right);
    motors.setSpeeds(-100, 100);                          // rotating Left 
    sensors.calibrate();                                  // collecting data light and darkness 
  }
  STOP();                                                 // stop 
  pinMode(LED, HIGH);                                     // light on 
  hitCount[0] = 9; hitCount[1] = 9; hitCount[2] = 9;      // setting up the hit count to diffearnt values at the begging
  buttonPressAndCountDown();                              // waiting for second press 
  motors.setSpeeds(-(lwheel + change), (rwheel + change));// turns left first turn to hit the wall
  delay(600);
}

void loop()
{                                                         // might get rid of this and go straing into the buttonPressAndCountDown()
  if (button.isPressed())                                 // if button is pressed, stop and wait for another press to go again
  {                                                                   
    button.waitForRelease();                              // waits for the button to be released
    buttonPressAndCountDown();                            // function to start a countdown before starting 
  }
  
  
  sensors.read(sensor_values);

  if (sensor_values[1] > sensorThreshold || sensor_values[2] > sensorThreshold)
  { 
    hitCount[2] = hitCount[1];
    hitCount[1] = hitCount[0];
    hitCount[0] = 0; // 0 = LEFT 1 = RIGHT
    leftCount++;
    rightCount = 0;
    
    while (sensor_values[1] > sensorThreshold || sensor_values[2] > sensorThreshold)
    { // rotate right 
      motors.setSpeeds((lwheel + change), -(rwheel + change));
      delay(200);
      sensors.read(sensor_values);
      //if (sensor_values[1] > sensorThreshold) 
      //{
        //Serial.println("Collision on the Left");
        //rightCount ++;
      //}
    }
    delay(600);
    Serial.println("Collision on the Left");
    cornerCounter(leftCount, rightCount);
  }
  if (sensor_values[4] > sensorThreshold || sensor_values[3] > sensorThreshold)
  {
    hitCount[2] = hitCount[1];
    hitCount[1] = hitCount[0];
    hitCount[0] = 1; // 0 = LEFT 1 = RIGHT
    rightCount++;
    leftCount = 0;
    
    while (sensor_values[4] > sensorThreshold || sensor_values[3] > sensorThreshold)
    { // rotate left
      motors.setSpeeds(-(lwheel + change), (rwheel + change));
      delay(200);
      sensors.read(sensor_values);
      
      //if (sensor_values[4] > sensorThreshold) 
      //{
        //Serial.println("Collision on the Right");
        //rightCount ++;
      //}
    }                                                     //
    delay(500);                                           // different delay time used to account for slower motor on the left
    Serial.println("Collision on the Right");
    cornerCounter(leftCount, rightCount);
  }                                                       
  
  ultrasonicSensor();                                     // calls the ulrasonicSensor function that sends out pings
  
  if(objectFound)                                         // if sensors pick anything up and object found has been set to true
  {
    Serial.println("objectFound: ");                      // prints line 
    STOP();                                               // stop motors
    delay(5000);                                          // delay before moving away
    motors.setSpeeds(100,-100);                           // turn not figgured out how far yet 180 probs
    delay(2000);
    objectFound = false;                                  // sets object found to false so it can move on
  }
  else
  {
    motors.setSpeeds(100,100);                            // otherwise it will keep going without a real delay 
    delay(50);
    sensors.read(sensor_values);                          // keeps reading in vlaues from the sensors 
  }
}

void buttonPressAndCountDown()                            // waits for a button press to count down and turn light on and off
{
  digitalWrite(LED, HIGH);                                // light on 
  button.waitForButton();                                 // waits for another button press 
  digitalWrite(LED, LOW);                                 // light off
                                                          // play audible countdown
  for (int i = 0; i < 3; i++)                             // count of 3
  {
    delay(1000);                                          // delay before each note is played  
    buzzer.playNote(NOTE_G(3), 200, 15);                  // plays a note 
  }
  delay(1000);                                                          
  buzzer.playNote(NOTE_G(4), 500, 15);                    // third and final note played
  delay(1000);                                            // done
}

//void room()
//{
  // so once the zumo thinks its in a room could do a 180 turn looking for a person or follow the room around and exit once it has found a person
  // or empty room 
  // message saying its in the room 
  // message saying it has left the room 
//}

void cornerCounter(int leftCount, int rightCount)
{ 
 
  if (leftCount >= 3 || rightCount >= 3)
  {
    Serial.println("Dead End");
    //deadEnd = true;
  }
  else if (hitCount[0] == 0 && hitCount[1] == 1 && hitCount[2] == 1) // 0 = LEFT 1 = RIGHT
  {
    Serial.println("Turning Corner Left");
  }
  else if (hitCount[0] == 1 && hitCount[1] == 0 && hitCount[2] == 0)
  {
    Serial.println("Turning Corner Right"); 
  }
  else if (hitCount[0] == 1 && hitCount[1] == 0 || hitCount[0] == 0 && hitCount[1] == 1)
  {
    Serial.println("In a corridor"); 
  }
}

void ultrasonicSensor()
{
  if (millis() >= pingTimer) 
  {                                                       // pingSpeed milliseconds since last ping, do another ping.
    pingTimer += pingSpeed;                               // Set the next ping time.
    sonar.ping_timer(echoCheck);                          // Send out the ping, calls "echoCheck" function every 24uS where you can check the ping status.
  }
}

void echoCheck()                                          // echoCheck is called within the function ultrasonicSensor to check recieved pings and checks for objects 
{ 
  if (sonar.check_timer())                                // Checking to see if the ping was received.
  { 
    Serial.print("Ping: ");
    Serial.print(sonar.ping_result / US_ROUNDTRIP_CM);    // Ping returned, uS result in ping_result, convert to cm with US_ROUNDTRIP_CM.
    Serial.println("cm");
    int foundSomething = (sonar.ping_result / US_ROUNDTRIP_CM);
    if (foundSomething <= 15 )                            // setting the object found status to true when something is under the 15cm radious
    {
      objectFound = true;                                 // sets objectFound to true if the sensors find something within 15cm
    }
    else
    {
      objectFound = false;                                // else it stays false and objectFound hasn't found anything
    }
  }
}

//bool journeyDefine()
//{
//  if (deadEnd == true)
//  {
//    Serial.println(" Returning to base");
//    // link to optimasation function
//  }
//}
  // function 
  // have a system that defines the jurney e.g. first trip true/false, on the way back true/false 
  // then if on the way back and entered a room with a person turn on LED pin 13 or something
