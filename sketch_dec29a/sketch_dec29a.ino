#include <ZumoBuzzer.h>/////might get rid of 
#include <Pushbutton.h> 
#include <QTRSensors.h>
#include <ZumoMotors.h>
#include <ZumoReflectanceSensorArray.h>
#include <NewPing.h>

#define LED 13
#define NUM_SENSORS 6 // dont know if i need this

#define TRIGGER_PIN   4  // Arduino pin tied to trigger pin on ping sensor. 
#define ECHO_PIN      5  // Arduino pin tied to echo pin on ping sensor.
#define MAX_DISTANCE 100 // Maximum distance we want to ping for (in centimeters). Maximum sensor distance is rated at 400-500cm.

unsigned int sensor_values[NUM_SENSORS];
unsigned int pingSpeed = 50; // How frequently are we going to send out a ping (in milliseconds). 50ms would be 20 times a second.
unsigned long pingTimer;     // Holds the next ping time.

ZumoBuzzer buzzer;
ZumoMotors motors; 
Pushbutton button(ZUMO_BUTTON); // pushbutton
ZumoReflectanceSensorArray sensors(QTR_NO_EMITTER_PIN);
NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE); // NewPing setup of pins and maximum distance.

int sensorThreshold = 500;

int change = 50, lwheel = 70, rwheel = 70;
bool objectFound = false;
 
void setup()
{
  Serial.begin(9600);
  button.waitForButton();
  pingTimer = millis(); // Start now.
  
  // the calibration code I didn't need but now seem to need
  unsigned long startTime = millis();
  while(millis() - startTime < 10000)   // make the calibration take 10 seconds
  {
    motors.setSpeeds(100,-100); // rotating   
    sensors.calibrate(); // collecting data light and darkness 
  }
  motors.setSpeeds(0,0); //stop 
  
  pinMode(LED, HIGH); // light on 
  buttonPressAndCountDown(); // waiting for second press 
}

void loop()
{ // might get rid of this and go straing into the buttonPressAndCountDown()
  if (button.isPressed())
  {
    // if button is pressed, stop and wait for another press to go again
    motors.setSpeeds(0, 0); //changed from 0 to 300
    button.waitForRelease();
    buttonPressAndCountDown();
  }
   
  sensors.read(sensor_values);

  if (sensor_values[0] > sensorThreshold || sensor_values[1] > sensorThreshold)
  {
    while (sensor_values[0] > sensorThreshold || sensor_values[1] > sensorThreshold)
    { // rotate right 
      motors.setSpeeds((lwheel + change), -(rwheel + change));
      sensors.read(sensor_values);
      delay(500);
      if (sensor_values[0] > sensorThreshold)
      {
        int count = 0;
        Serial.println("Collision on the Left");
//        count ++;
//        if (count = 2)
//        {
//          //hitting a corner 
//        } //maybe but this in its own function
      }
    }
  }
  if (sensor_values[4] > sensorThreshold || sensor_values[3] > sensorThreshold)
  {
    while (sensor_values[4] > sensorThreshold || sensor_values[3] > sensorThreshold)
    { // rotate left
      motors.setSpeeds(-(lwheel + change), (rwheel + change));
      sensors.read(sensor_values);
      delay(500);
      if (sensor_values[4] > sensorThreshold) 
      {
        Serial.println("Collision on the Right");
      }
    }
  }
  //else
  //{
    //while (sensor_values[0] < 100 && sensor_values[4] < 100)//neither sensor detects
    //{                                                        
      
      ultrasonicSensor();
      
      if(objectFound)
      {
        Serial.println("objectFound: ");
        motors.setSpeeds(0,0);
        delay(5000);
        motors.setSpeeds(100,-100);
        delay(2000);
        objectFound = false;
      }
      else
      {
        motors.setSpeeds(100,100);                             
        delay(50);
        sensors.read(sensor_values);  
      }
      
    //}
  //}
}

void buttonPressAndCountDown()
{
  digitalWrite(LED, HIGH);
  button.waitForButton();
  digitalWrite(LED, LOW);
   
  // play audible countdown
  for (int i = 0; i < 3; i++)
  {
    delay(1000);
    buzzer.playNote(NOTE_G(3), 200, 15);
  }
  delay(1000);
  buzzer.playNote(NOTE_G(4), 500, 15);  
  delay(1000);
}

void ultrasonicSensor()
{
  if (millis() >= pingTimer) 
  {   
    // pingSpeed milliseconds since last ping, do another ping.
    pingTimer += pingSpeed;      // Set the next ping time.
    sonar.ping_timer(echoCheck); // Send out the ping, calls "echoCheck" function every 24uS where you can check the ping status.
  }
}
void echoCheck() 
{ // Timer2 interrupt calls this function every 24uS where you can check the ping status.
  // Don't do anything here!
  if (sonar.check_timer()) 
  { // This is how you check to see if the ping was received.
    Serial.print("Ping: ");
    Serial.print(sonar.ping_result / US_ROUNDTRIP_CM); // Ping returned, uS result in ping_result, convert to cm with US_ROUNDTRIP_CM.
    Serial.println("cm");
    int foundSomething = (sonar.ping_result / US_ROUNDTRIP_CM);
    if (foundSomething <= 15 )
    {
      objectFound = true;
    }
    else
    {
      objectFound = false;
    }
    
  }
}
