#include <ZumoBuzzer.h>/////might get rid of 
#include <Pushbutton.h> 
#include <QTRSensors.h>
#include <ZumoMotors.h>
#include <ZumoReflectanceSensorArray.h>

#define LED 13
#define NUM_SENSORS 6

unsigned int sensor_values[NUM_SENSORS];

ZumoBuzzer buzzer;
ZumoMotors motors; 
Pushbutton button(ZUMO_BUTTON); // pushbutton
 
ZumoReflectanceSensorArray sensors(QTR_NO_EMITTER_PIN);
int change = 50, lwheel = 60, rwheel = 60;
void waitForButtonAndCountDown()
{
  digitalWrite(LED, HIGH);
  button.waitForButton();
  digitalWrite(LED, LOW);
   
  // play audible countdown
  for (int i = 0; i < 3; i++)
  {
    delay(1000);
    buzzer.playNote(NOTE_G(3), 200, 15);
  }
  delay(1000);
  buzzer.playNote(NOTE_G(4), 500, 15);  
  delay(1000);
}
 
void setup()
{
  Serial.begin(9600);
  pinMode(LED, HIGH);  
  waitForButtonAndCountDown();
}

void loop()
{
  if (button.isPressed())
  {
    // if button is pressed, stop and wait for another press to go again
    motors.setSpeeds(0, 0); //changed from 0 to 300
    button.waitForRelease();
    waitForButtonAndCountDown();
  }
   
  sensors.read(sensor_values);
  //turning right
  if (sensor_values[0] > 100 || sensor_values[1] > 100)
  {
    while (sensor_values[0] > 100 || sensor_values[1] > 100)
    {
      motors.setSpeeds((lwheel - change), -(rwheel + change));
      sensors.read(sensor_values);
    }
  }
  else if (sensor_values[4] > 100 || sensor_values[3] > 100)
  {
    while (sensor_values[4] > 100 || sensor_values[3] > 100)
    {
      motors.setSpeeds(-(lwheel + change), (rwheel - change));
      sensors.read(sensor_values);
    }
  }
  else
  {
    while (sensor_values[0] < 100 && sensor_values[6] < 100)//neither sensor detects
    {                                                        
      motors.setSpeeds(100,100);                             
      delay(50);
      sensors.read(sensor_values);
    }
  }
}
