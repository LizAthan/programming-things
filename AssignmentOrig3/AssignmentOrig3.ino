#include <ZumoBuzzer.h>
#include <Pushbutton.h> 
#include <ZumoMotors.h>
#include <QTRSensors.h>
#include <ZumoReflectanceSensorArray.h>
#include <NewPing.h>

#define LED 13                             
#define TRIGGER_PIN  4                                    // Arduino pin tied to trigger pin on ping sensor. 
#define ECHO_PIN     5                                    // Arduino pin tied to echo pin on ping sensor.
#define MAX_DISTANCE 100                                  // Maximum distance we want to ping for (in centimeters). Maximum sensor distance is rated at 400-500cm.
#define STOP()(motors.setSpeeds(0,0))                     // Mini function to stop zumo
#define FORWARD()(motors.setSpeeds(100,100))
#define LEFT()(motors.setSpeeds(-(lwheel + change), (rwheel + change)))
#define RIGHT()(motors.setSpeeds((lwheel + change), -(rwheel + change)))
#define NUM_SENSORS  6  
  
unsigned int sensor_values[NUM_SENSORS];
unsigned int pingSpeed = 50;                              // How frequently are we going to send out a ping (in milliseconds). 50ms would be 20 times a second.
unsigned long pingTimer;                                  // Holds the next ping time.

ZumoBuzzer buzzer;
ZumoMotors motors; 
Pushbutton button(ZUMO_BUTTON);                           // pushbutton
ZumoReflectanceSensorArray sensors(QTR_NO_EMITTER_PIN);
NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE);       // NewPing setup of pins and maximum distance.

int timer = 0;
int sensorThreshold = 500;                                // 500 or 100 depending on lighting
int change = 50, lwheel = 80, rwheel = 80;                // change allows motor speed to be increased/decreased, motor speed standard value
int leftCount = 0, rightCount = 0;                        // counts for every time left or right is hit
int roomCount = 0;                                        // basically like a room ID
int hitCount [3];                                         // hit counter array records last 3 hits for sequence checks
String rooms[10];                                         // stores what it has found for each room
bool searchingRoom = false;
bool objectFound = false;                                 // used for finding a person currently no person found.
//bool deadEnd = false;


void setup()
{
  Serial.begin(9600);
  button.waitForButton();
  pingTimer = millis();                                   // Start now.
  
  // Calibration code I didn't need but now seem to need
  unsigned long startTime = millis();
  
  while(millis() - startTime < 10000)                     // make the calibration take 10 seconds
  {                                                       // motor/setSpeeds(Left, Right);
    motors.setSpeeds(-100, 100);                          // calibration rotating Left
    sensors.calibrate();                                  // collecting data light and darkness 
  }
  STOP();                                                 // stop 
  pinMode(LED, HIGH);                                     // light on 
  hitCount[0] = 9; hitCount[1] = 9; hitCount[2] = 9;      // setting up the hit count to diffearnt values at the beginning
  buttonPressAndCountDown();                              // waiting for second press 
  LEFT();                                                 // turns left first turn to find and hit a wall
  delay(600);
}

void loop()
{                                                         // might get rid of this and go straing into the buttonPressAndCountDown()
  if (button.isPressed())                                 // if button is pressed, stop and wait for another press to go again
  {                                                                   
    button.waitForRelease();                              // waits for the button to be released
    buttonPressAndCountDown();                            // function to start a countdown before starting 
  }
  timer++;
    
  sensors.read(sensor_values);
  //checkTimer();
  if (sensor_values[1] > sensorThreshold || sensor_values[2] > sensorThreshold)
  { 
    hitCount[2] = hitCount[1];
    hitCount[1] = hitCount[0];
    hitCount[0] = 0; // 0 = LEFT 1 = RIGHT
    leftCount++;
    rightCount = 0;
    
    while (sensor_values[1] > sensorThreshold || sensor_values[2] > sensorThreshold)
    { // rotate right 
      RIGHT();//motors.setSpeeds((lwheel + change), -(rwheel + change));
      delay(200);
      sensors.read(sensor_values);
    }
    delay(600);                                           // different delay time used to account for slower motor on the left
    Serial.println("Collision on the Left");
    cornerCounter(leftCount, rightCount);
    timer = 0;
  }
  if (sensor_values[4] > sensorThreshold || sensor_values[3] > sensorThreshold)
  {
    hitCount[2] = hitCount[1];
    hitCount[1] = hitCount[0];
    hitCount[0] = 1; // 0 = LEFT 1 = RIGHT
    rightCount++;
    leftCount = 0;
    
    while (sensor_values[4] > sensorThreshold || sensor_values[3] > sensorThreshold)
    { // rotate left
      LEFT();// motors.setSpeeds(-(lwheel + change), (rwheel + change));
      delay(200);
      sensors.read(sensor_values);
    }                                                     
    delay(500);                                           // different delay time used to account for slower motor on the left
    Serial.println("Collision on the Right");
    cornerCounter(leftCount, rightCount);
    timer = 0;
  } 

  ultrasonicSensor();
  checkTimer();
  
  if(objectFound == true && searchingRoom == true)                                         // if sensors pick anything up and object found has been set to true
  {

    Serial.print("room: ");
    Serial.println(roomCount);
    rooms[roomCount] = "Person Found";    
    Serial.println("Object Found");                       // prints that it has found something "hidden object"
    STOP();                                               // stop motors
    delay(5000);                                          // delay before moving away
    motors.setSpeeds(100,-100);                           // turn not figgured out how far yet 180 probs

    LEFT(); // right now its just to show its in a room and found someone
    delay (5000);
    RIGHT();
    delay(5000);//2000
    
    objectFound = false;   
  }
  
  else
  {
    FORWARD();                                            
    delay(50);                                            // otherwise it will keep going without a real delay 
    sensors.read(sensor_values);                          // keeps reading in vlaues from the sensors 
  }
}

void buttonPressAndCountDown()                            // waits for a button press to count down and turn light on and off
{
  digitalWrite(LED, HIGH);                                // light on 
  button.waitForButton();                                 // waits for another button press 
  digitalWrite(LED, LOW);                                 // light off
                                                          // play audible countdown
  for (int i = 0; i < 3; i++)                             // count of 3
  {
    delay(1000);                                          // delay before each note is played  
    buzzer.playNote(NOTE_G(3), 200, 15);                  // plays a note 
  }
  delay(1000);                                                          
  buzzer.playNote(NOTE_G(4), 500, 15);                    // third and final note played
  delay(1000);                                            // done
}

//void room()
//{ 
// searchingRoom = true; 
// bool tempObjectFound = false;

//  LEFT();
//  delay(800);
//  RIGHT();
//  delay (800);
  
//  if(objectFound)                                         // if sensors pick anything up and object found has been set to true
//  {
//    rooms[roomCount] = "Person Found";    
//    Serial.println("Object Found");                       // prints that it has found something "hidden object"
//    STOP();                                               // stop motors
//    delay(5000);                                          // delay before moving away
//    motors.setSpeeds(100,-100);                           // turn not figgured out how far yet 180 probs
//    delay(5000);//2000
//    
//    objectFound = false;   
//  }
//  else
//  {
//    rooms[roomCount] = "Empty";
//    Serial.println("Empty");
//  }
//}   
  // do search of room 
  // so once the zumo thinks its in a room could do a 180 turn looking for a person or follow the room around and exit once it has found a person
  // or empty room 
  // message saying its in the room 
  // message saying it has left the room 


void cornerCounter(int leftCount, int rightCount)
{ 
  if (leftCount >= 3 || rightCount >= 3)
  {
    Serial.println("Dead End");
    //deadEnd = true;
  }
  else if (hitCount[0] == 0 && hitCount[1] == 1 && hitCount[2] == 1) // 0 = LEFT 1 = RIGHT
  {
    Serial.println("Turning Corner Left");
    Serial.print("Timer turning corner Left: ");
    Serial.println(timer);
  }
  else if (hitCount[0] == 1 && hitCount[1] == 0 && hitCount[2] == 0)
  {
    Serial.println("Turning Corner Right");
    Serial.print("Timer turning corner Right: ");
    Serial.println(timer);
    searchingRoom = false;
  }
  else if (hitCount[0] == 1 && hitCount[1] == 0 || hitCount[0] == 0 && hitCount[1] == 1)
  {
    Serial.println("In a corridor"); 
    Serial.print("Corridor Timer: ");
    Serial.println(timer);
    searchingRoom = false;
  }
}

void checkTimer()
{
  if (timer > 35)
  {
    STOP();
    Serial.print("Stopped timer over  35  :  ");
    Serial.println(timer);
    Serial.println("Found Room");
    delay(900);
    timer = 0;  
    Serial.println("Searching");
    searchingRoom = true;
    //room();
   }
}

void ultrasonicSensor()
{
  if (millis() >= pingTimer) 
  {                                                       // pingSpeed milliseconds since last ping, do another ping.
    pingTimer += pingSpeed;                               // Set the next ping time.
    sonar.ping_timer(echoCheck);                          // Send out the ping, calls "echoCheck" function every 24uS where you can check the ping status.
  }
}

void echoCheck()                                          // echoCheck is called within the function ultrasonicSensor to check recieved pings and checks for objects 
{ 
  if (sonar.check_timer())                                // Checking to see if the ping was received.
  { 
    int foundSomething = (sonar.ping_result / US_ROUNDTRIP_CM);
    if (foundSomething <= 15 )                            // setting the object found status to true when something is under the 15cm radious
    {
      Serial.print("Ping: ");
      Serial.print(sonar.ping_result / US_ROUNDTRIP_CM);    // Ping returned, uS result in ping_result, convert to cm with US_ROUNDTRIP_CM.
      Serial.println("cm");
      objectFound = true;                                 // sets objectFound to true if the sensors find something within 15cm
    }
    else
    {
      objectFound = false;                                // else it stays false and objectFound hasn't found anything
    }
  }
}



// so get the compas to get  the heading its going down the corridor originally 
// then when it thinks it has detected a room 
// check the last wall it hit if its the right wall/ right sensors try and line up perfectly straight into the room 
// do a 360 turn then 
// then possibly find the wall follow the wall around 
// then check the heading before you were in the room 
// make sure the zumo is facing that way










//bool journeyDefine()
//{
//  if (deadEnd == true)
//  {
//    Serial.println(" Returning to base");
//    // link to optimasation function
//  }
//}
  // function 
  // have a system that defines the jurney e.g. first trip true/false, on the way back true/false 
  // then if on the way back and entered a room with a person turn on LED pin 13 or something
